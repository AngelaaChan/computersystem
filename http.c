/*
** http-server.c
*/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <strings.h>
#include <sys/select.h>
#include <sys/sendfile.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>




// cookie for user, formtted with userID
static char const * const HTTP_200_FORMAT = "HTTP/1.1 200 OK\r\n\
Content-Type: text/html\r\n\
Content-Length: %ld\r\n\
Set-Cookie: id = %d \r\n\r\n";




static char const * const HTTP_400 = "HTTP/1.1 400 Bad Request\r\nContent-Length: 0\r\n\r\n";
static int const HTTP_400_LENGTH = 47;
static char const * const HTTP_404 = "HTTP/1.1 404 Not Found\r\nContent-Length: 0\r\n\r\n";
static int const HTTP_404_LENGTH = 45;



typedef enum
{
    GET,
    POST,
    UNKNOWN
} METHOD;



// struct User
struct User {
    int userID;
    char* username;
    char** keywords;
    int isOnline; // Online=1, OffLine=0
    int stage;

};


// malloc a newUser
struct User* newUser() {
    struct User* newUser = malloc(sizeof (struct User));
    newUser->userID = 0;
    newUser->username = NULL;
    newUser->keywords = NULL;
    newUser->isOnline = 0;
    newUser->stage = 0;
    return newUser;
}



struct User* curr(struct User* user_arr[], int sockfd)
{
	for (int i=0; i<2; i++) {
		if (user_arr[i]->userID == sockfd)
		{				
			return user_arr[i];
			printf("init user %d , with sockfd %d", i, sockfd);
		}
	}
	return NULL;
}


void init_user_id(struct User* user_arr[], int sockfd)
{
	for (int i = 0; i < 2; i++)
	{
		if (user_arr[i]->userID == 0)
		{
			user_arr[i]->userID = sockfd;
            return;
            
			printf("user %d is current user, with sockfd %d", i, sockfd);
		    
        }
	}
}

/*

void match_words(struct User* user_arr[]){

    char* words1[];

    for(int i=0; i<2; i++){
        if (i==0){

        }
        else(i==1){}
    }
}


*/


void simple_load_html(int sockfd, char* buff, int n, char* html) 
{
            	if (write(sockfd, buff, n) < 0)
            {
                perror("write");
                return ;
            }
            // send the file
            int filefd = open("1_intro.html", O_RDONLY);
            do
            {
                n = sendfile(sockfd, filefd, NULL, 2048);
            }
            while (n > 0);
            if (n < 0)
            {
                perror("sendfile");
                close(filefd);
                return ;
            }
            close(filefd);

}











static bool handle_http_request(int sockfd, struct User* user_arr[])
{






	// set sockfd and cookie
	init_user_id(user_arr, sockfd);



	// cast current user
	struct User* curr_user = curr(user_arr, sockfd);





    // try to read the request
    char buff[2049];
    int n = read(sockfd, buff, 2049);


    if (n <= 0)
    {
        if (n < 0)
            perror("read");
        else
            printf("socket %d close the connection\n", sockfd);
        return false;
    }

    // terminate the string
    buff[n] = 0;

    char * curr = buff;

    // parse the method
    METHOD method = UNKNOWN;
    if (strncmp(curr, "GET ", 4) == 0)
    {
        curr += 4;
        method = GET;
    }
    else if (strncmp(curr, "POST ", 5) == 0)
    {
        curr += 5;
        method = POST;
    }
    else if (write(sockfd, HTTP_400, HTTP_400_LENGTH) < 0)
    {
        perror("write");
        return false;
    }

    printf("buff1: \n");
    printf("%s\n",buff);

    // sanitise the URI
    while (*curr == '.' || *curr == '/')
        ++curr;


    // assume the only valid request URI is "/" but it can be modified to accept more files
    printf("method is %d, curr_user->ID is %d, stage is %d \n",method, curr_user->userID, curr_user->stage );
    if (*curr == ' ')
        if (method == GET && curr_user->stage==0)
        {

            curr_user->stage=1;

    		printf("buff1: \n");
    		printf("%s\n",buff);
            // get the size of the file
            struct stat st;
            stat("1_intro.html", &st);
            n = sprintf(buff, HTTP_200_FORMAT, st.st_size, sockfd);
            simple_load_html(sockfd, buff, n, "1_intro.html");
            printf(" userID: %d  at stage 0 \n", curr_user->userID);
            
          
            // send the header first
    /*        if (write(sockfd, buff, n) < 0)
            {
                perror("write");
                return false;
            }
            // send the file
            int filefd = open("1_intro.html", O_RDONLY);
            do
            {
                n = sendfile(sockfd, filefd, NULL, 2048);
            }
            while (n > 0);
            if (n < 0)
            {
                perror("sendfile");
                close(filefd);
                return false;
            }
            close(filefd);
	*/	


            
        }







        else if (method == POST && curr_user->stage==1 )
        {
            // locate the username, it is safe to do so in this sample code, but usually the result is expected to be
            // copied to another buffer using strcpy or strncpy to ensure that it will not be overwritten.
            // locate the username, it is safe to do so in this sample code, but usually the result is expected to be
            // copied to another buffer using strcpy or strncpy to ensure that it will not be overwritten.
            char * username = strstr(buff, "user=") + 5;
            int username_length = strlen(username);
            // the length needs to include the ", " before the username
            long added_length = username_length+3;

            // get the size of the file
            struct stat st;
            stat("2_start.html", &st);
            // increase file size to accommodate the username
            long size = st.st_size + added_length;
            n = sprintf(buff, HTTP_200_FORMAT, size, sockfd);
            // send the header first
            if (write(sockfd, buff, n) < 0)
            {
                perror("write");
                return false;
            }
            // read the content of the HTML file
            int filefd = open("2_start.html", O_RDONLY);
            n = read(filefd, buff, 2048);
            if (n < 0)
            {
                perror("read");
                close(filefd);
                return false;
            }
            close(filefd);
            // move the trailing part backward


            int p1, p2;
            for (p1 = size - 1, p2 = p1 - added_length; p1 >= size - 18; --p1, --p2)
                buff[p1] = buff[p2];
            ++p2;
            buff[++p2]='<';
            buff[++p2]='p';
            buff[++p2]='>';
            buff[++p2]=' ';
            // put the separator
            // copy the username
            strncpy(buff + p2, username, username_length);


             printf(" userID: %d  at stage 1 \n", curr_user->userID);



            if (write(sockfd, buff, size) < 0)
            {
                perror("write");
                return false;
            }
            curr_user->stage=2;

        }
        else if (curr_user->stage==2)
        {

        	 printf(" userID: %d  at stage 2 \n", curr_user->userID);


        	if (method == GET)
        	{   
        		curr_user->stage == 3;
        	}
        	else if(method == POST)
        	{
        		curr_user->stage == 7;
        	}	
        }
/*        else if (curr_user->stage==3)
        {

        }
        else if (curr_user->stage==7) 
        {
        	simple_load_html(sockfd, buff, n, "7_gameover.html");	
        }

*/

        else	
            // never used, just for completeness
            fprintf(stderr, "no other methods supported");
    // send 404
    else if (write(sockfd, HTTP_404, HTTP_404_LENGTH) < 0)
    {
        perror("write");
        return false;
    }

    return true;
}

int main(int argc, char * argv[])
{
    if (argc < 3)
    {
        fprintf(stderr, "usage: %s ip port\n", argv[0]);
        return 0;
    }

    // create TCP socket which only accept IPv4
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // reuse the socket if possible
    int const reuse = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) < 0)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // create and initialise address we will listen on
    struct sockaddr_in serv_addr;
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    // if ip parameter is not specified
    serv_addr.sin_addr.s_addr = inet_addr(argv[1]);
    serv_addr.sin_port = htons(atoi(argv[2]));

    // bind address to socket
    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    // listen on the socket
    listen(sockfd, 5);

    // initialise an active file descriptors set
    fd_set masterfds;
    FD_ZERO(&masterfds);
    FD_SET(sockfd, &masterfds);
    // record the maximum socket number
    int maxfd = sockfd;






    //global variable for 2 users
    struct User* user_arr[2];
    struct User* user1 = newUser();
    struct User* user2 = newUser();
    user_arr[0] = user1;
    user_arr[1] = user2;





    while (1)
    {
        // monitor file descriptors
        fd_set readfds = masterfds;
        if (select(FD_SETSIZE, &readfds, NULL, NULL, NULL) < 0)
        {
            perror("select");
            exit(EXIT_FAILURE);
        }

        // loop all possible descriptor
        for (int i = 0; i <= maxfd; ++i)
            // determine if the current file descriptor is active
            if (FD_ISSET(i, &readfds))
            {
                // create new socket if there is new incoming connection request
                if (i == sockfd)
                {
                    struct sockaddr_in cliaddr;
                    socklen_t clilen = sizeof(cliaddr);
                    int newsockfd = accept(sockfd, (struct sockaddr *)&cliaddr, &clilen);
                    if (newsockfd < 0)
                        perror("accept");
                    else
                    {
                        // add the socket to the set
                        FD_SET(newsockfd, &masterfds);
                        // update the maximum tracker
                        if (newsockfd > maxfd)
                            maxfd = newsockfd;
                        // print out the IP and the socket number
                        char ip[INET_ADDRSTRLEN];
                        printf(
                            "new connection from %s on socket %d\n",
                            // convert to human readable string
                            inet_ntop(cliaddr.sin_family, &cliaddr.sin_addr, ip, INET_ADDRSTRLEN),
                            newsockfd
                        );
                    }
                }
                // a request is sent from the client
                else if (!handle_http_request(i, user_arr))
                {
                    close(i);
                    FD_CLR(i, &masterfds);
                }
            }
    }

    return 0;
}



